# plugin-country-city-cpt

Plugin para mostrar Pais, Estado y Ciudad de manera dinámica como Custom Post Type. Genera un Shortcode para buscar este CPT en el Front del producto. Repositorio privado https://gitlab.com/diurvan/diu-country-city-cpt

![Imagen de Configuración](https://gitlab.com/diurvan/plugin-country-city-cpt/-/raw/main/country-cpt-configuracion.png)
![Imagen de CPT](https://gitlab.com/diurvan/plugin-country-city-cpt/-/raw/main/country-cpt-customfields.png)
![Imagen de Front Shortcode](https://gitlab.com/diurvan/plugin-country-city-cpt/-/raw/main/country-cpt-shortcode-front.png)

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
